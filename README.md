# Install and run the development libraries

```
npm install;
npm start;
```

# Add the guntenslider plugin

```
docker run -it --rm \
    --volumes-from voile-wordpress \
    --network container:voile-wordpress \
    -e HOME=/tmp \
    -e WORDPRESS_DB_HOST=database \
    -e WORDPRESS_DB_USER=voile \
    -e WORDPRESS_DB_PASSWORD=voile \
    -e WORDPRESS_DB_NAME=voile \
    wordpress:cli plugin install gutenslider --activate;
```