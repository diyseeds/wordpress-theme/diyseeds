#!/bin/bash

# install WordPress
docker-entrypoint.sh apache2;

# get the latest voile theme
cd /tmp/

echo "[init.sh] install voile from repo"
wget https://framagit.org/api/v4/projects/76718/packages/generic/voile/latest/voile.zip;
unzip voile.zip -d voile;
chown -R www-data:www-data voile;
rm -rf /var/www/html/wp-content/themes/voile;
mv voile /var/www/html/wp-content/themes/;

echo "[init.sh] install block opacity from repo"
wget https://framagit.org/api/v4/projects/77587/packages/generic/lecoqlibre-block-opacity/latest/lecoqlibre-block-opacity.zip;
unzip lecoqlibre-block-opacity.zip;
chown -R www-data:www-data lecoqlibre-block-opacity;
rm -rf /var/www/html/wp-content/plugins/lecoqlibre-block-opacity;
mv lecoqlibre-block-opacity /var/www/html/wp-content/plugins/;

# create uploads directory (into /var/www/html/wp-content/)
echo "[init.sh] create uploads directory";
mkdir -p /var/www/html/wp-content/uploads/;
chown -R www-data:www-data /var/www/html/wp-content/uploads/;

# manage themes
cd /var/www/html/;

echo "[init.sh] activate diyseeds theme";
runuser -u www-data -- wp theme activate diyseeds;
# runuser -u www-data -- wp theme delete --all; # won't delete active theme

echo "[init.sh] activate Lecoqlibre's block opacity plugin";
runuser -u www-data -- wp plugin activate lecoqlibre-block-opacity;

echo "[init.sh] delete pre-installed plugins"
runuser -u www-data -- wp plugin delete akismet hello;

echo "[init.sh] install and activate gutenberg plugin"
runuser -u www-data -- wp plugin install gutenberg --activate;

echo "[init.sh] install and activate gutenslider plugin"
runuser -u www-data -- wp plugin install gutenslider --activate;

echo "[init.sh] install and activate french language"
runuser -u www-data -- wp language core install fr_FR;
runuser -u www-data -- wp language core activate fr_FR;

# cd /var/www/html/diyseeds/htdocs/wp-content/plugins/;

# echo "install plugins to /var/www/html/diyseeds/htdocs/wp-content/plugins/"
# all-in-one-seo-pack.3.3.4, jetpack.8.1.1, newsletter.6.4.3, woocommerce.3.9.1, woocommerce-gateway-paypal-express-checkout.1.6.18;

# cd /var/www/html/diyseeds/;

# echo "run composer in /var/www/html/diyseeds/";
# composer install;

# echo "create export directory in /var/www/html/diyseeds/htdocs/wp-content/plugins/diyseeds/";
# mkdir -p /var/www/html/diyseeds/htdocs/wp-content/plugins/diyseeds/export/;
# chown -R root:www-data /var/www/html/diyseeds/htdocs/wp-content/plugins/diyseeds/export/;
# chmod -R g+w /var/www/html/diyseeds/htdocs/wp-content/plugins/diyseeds/export/;

# try enabling apache2 headers mode
a2enmod headers;

# run apache
apache2-foreground;
