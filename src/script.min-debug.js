class LecoqlibreSwitch extends HTMLElement {

  static get observedAttributes() {
    return [ 'enabled' ];
  }

  constructor() {
    super();
    var template = document.createElement( 'template' );
    template.innerHTML = `
      <style type="text/css">
        :host(lecoqlibre-switch) {
          width: 0;
          opacity: 0;
          pointer-events: none;
        }
        :host(lecoqlibre-switch[enabled]) {
          width: 30px;
          opacity: 100;
          pointer-events: initial;
        }
      </style>
      <slot name="content"></slot>`;
    this.attachShadow( { mode: 'open' } );
    this.shadowRoot.appendChild( template.content.cloneNode( true ) );
  }

  get enabled() {
    return this.hasAttribute( 'enabled' );
  }

  set enabled( value ) {
    if ( value ) {
      this.setAttribute( 'enabled', '' );
    } else {
      this.removeAttribute( 'enabled' );
    }
  }

  isEnabled() {
    return this.hasAttribute( 'enabled' );
  }

  toggle() {
    this.enabled = ! this.enabled;
  }

  connectedCallback() {
    console.log( "lecoqlibre-switch connected" );
  }

}

customElements.define( 'lecoqlibre-switch', LecoqlibreSwitch ); // hjkfdsfds
// setTimeout( () => customElements.define( 'lecoqlibre-switch', LecoqlibreSwitch ), 1000 );

class LecoqlibreSwitcher extends HTMLElement {

    static get observedAttributes() {
      return [ 'for' ];
    }
  
    constructor() {
      super();
      var template = document.createElement( 'template' );
      template.innerHTML = `
        <style type="text/css">
          :host(lecoqlibre-switcher) {
            cursor: pointer
          }
        </style>
        <slot name="switcher"></slot>`;
      this.attachShadow( { mode: 'open' } );
      this.shadowRoot.appendChild( template.content.cloneNode( true ) );
  
      this.shadowRoot.querySelector( '[name="switcher"]' ).addEventListener( 'click', e => {
        this.toggle();
      } );
  
    }

    isEnabled() {
        return this.hasAttribute( 'enabled' );
    }
  
    toggle() {
        if( this.hasAttribute( 'for' ) ) {
            var target = document.getElementById( this.getAttribute( 'for' ) );
            target.toggle();
            if( target.isEnabled() )
                this.setAttribute( 'enabled', '' );
            else this.removeAttribute( 'enabled' );
        }
    }
  
    connectedCallback() {
      console.log( "lecoqlibre-switcher connected" );
    }
  
  }
  
  customElements.define( 'lecoqlibre-switcher', LecoqlibreSwitcher );
  // setTimeout( () => customElements.define( 'lecoqlibre-switcher', LecoqlibreSwitcher ), 1000 );
  