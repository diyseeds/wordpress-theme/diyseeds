<?php

add_action( 'init', function() {
    if( get_theme_mod( 'sidemenu_switcher_content' ) )
        set_theme_mod( 'sidemenu_switcher_content', '<div id="lecoqlibre-voile-sidemenu-switcher-image"></div>' );

    register_block_pattern(
        'lecoqlibre-diyseeds/organization',
        array(
            'title'       => __( 'Organization', 'lecoqlibre-diyseeds' ),
            'description' => _x( 'Organization.', 'Block pattern description', 'lecoqlibre-diyseeds' ),
            'content'     => "<!-- wp:group {\"className\":\"lecoqlibre-diyseeds-organization\"} -->\n<div class=\"wp-block-group lecoqlibre-diyseeds-organization\"><!-- wp:heading {\"level\":3,\"className\":\"is-style-uppercase\"} -->\n<h3 class=\"is-style-uppercase\">Organization name</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Organization description</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:list {\"className\":\"is-style-links\"} -->\n<ul class=\"is-style-links\"><li><a href=\"http://link\">organization link</a></li></ul>\n<!-- /wp:list --></div>\n<!-- /wp:group -->",
            'categories' => [ 'columns' ],
            'keywords' => [ 'diyseeds' ],
        )
    );

    register_block_style( 'core/separator', 
        array(
            'name'         => 'diyseeds-45px',
            'label'        => __( '45 pixels width', 'lecoqlibre-diyseeds' ),
        ),
    );

    register_block_style( 'core/separator', 
        array(
            'name'         => 'diyseeds-full-opaque',
            'label'        => __( 'Full width opaque', 'lecoqlibre-diyseeds' ),
        ),
    );

    $style = array(
        'name'         => 'diyseeds-uppercase',
        'label'        => __( 'Uppercase', 'lecoqlibre-diyseeds' ),
        'is_default'   => true,
    );
    register_block_style( 'core/post-title', $style );
    register_block_style( 'core/heading', $style );
    
    register_block_style( 'core/list', 
        array(
            'name'         => 'diyseeds-links',
            'label'        => __( 'Links', 'lecoqlibre-diyseeds' ),
        )
    );

    register_block_style( 'core/list', 
        array(
            'name'         => 'diyseeds-seeds',
            'label'        => __( 'Seeds', 'lecoqlibre-diyseeds' ),
        )
    );
} );

add_action( 'after_setup_theme', function() {

} );

/** Enqueue theme style or child theme style. */
add_action( 'wp_enqueue_scripts', function() {
    $styleFilename = '/style.min.css';
    $styleFile = get_stylesheet_directory_uri() . $styleFilename;
    wp_enqueue_style( 'lecoqlibre-diyseeds-style', $styleFile, array(), wp_get_theme()->get( 'Version' ) );
  }, 11 );